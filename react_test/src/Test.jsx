import React, { Component } from 'react'
class Test extends Component {
    constructor(){
        super();
        this.state = {
            data: [
                {
                    id: 1,
                    name: "bob",
                    age: 5
                },
                {
                    id: 2,
                    name: "jack",
                    age: 6
                }
            ],
            isActive: null,
            msg: 1,
            text: "Hide"
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(){
        this.setState(prevState => ({ msg: prevState.msg + 1}))
    }
    changeColor=(e)=>{
        if (e !== this.state.isActive) {
            alert(e)
            this.setState({isActive: e})
            this.setState({text: "red"})
        } 
        else {
            this.setState({isActive: null});
        }
    }
    show=(e)=>{
        alert(e.id + e.name + e.age + "fuck senglong")
    }
    render() {
    let list = this.state.data.map((d)=>
            <tr key={d.id} style={this.state.isActive === d.id ? { color: 'red' }: { color: '' }}>
                <td>{d.id}</td>
                <td>{d.name}</td>
                <td>{d.age}</td>
                <td><button style={this.state.isActive === d.id ? { color: 'red' }: { color: '' }} onClick={()=>this.changeColor(d.id)}>{d.id}</button></td>
            </tr>
    )
        return (
            <div style={{margin: 300}}>
                <button onClick={this.handleChange}>Add Object</button>
                <span>{this.state.msg}</span>
                <table>
                    {list}
                </table>
            </div>
        )
    }
}
export default Test;
