import React, { Component } from 'react'
import PropsComponent from './PropsComponent'

export default class StateComponent extends Component {
    constructor(){
        super();
        this.state = {
            book: [
                {
                    id: 1,
                    name: "me"
                },
                { 
                    id: 2,
                    name: "you"
                }
            ],
            activePage: 15
        }
    }
    render() {
        let list = this.state.book.map((d)=>
            <PropsComponent key={d.id} b={d}/>
        )

        return (
            <div>
                <h1 style={{background: "yellow"}}>Hello</h1>
                <table>
                    {list}
                </table>
                <Pagination
      prevPageText='prev'
      nextPageText='next'
      firstPageText='first'
      lastPageText='last'
      activePage={this.state.activePage}
      itemsCountPerPage={PER_PAGE}
      totalItemsCount={TOTAL_COUNT}
      onChange={this.handlePageChange}
    />
            </div>
        )
    }
}
