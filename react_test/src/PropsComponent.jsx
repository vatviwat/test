import React, { Component } from 'react'

export default class PropsComponent extends Component {
    constructor(){
        super();
        this.state = {
            textColor: "",
            text: "Hide",
            isActive: null,
        }
        this.onChange = this.onChange.bind(this);
    }
    onChange(event){
        if (event !== this.state.isActive) {
            this.setState({isActive: event,textColor: "red",text: "Unhide"})
        } 
        else {
            this.setState({isActive: null,textColor: "",text: "Hide"});
        }
    }
    render() {
        return (
            <div>
                <tr>
                    <td style={{background: this.state.textColor}}>{this.props.b.id}</td>
                    <td style={{background: this.state.textColor}}>{this.props.b.name}</td>
                    <td>
        <button style={{background: this.state.textColor}} onClick={this.onChange}>{this.state.text}</button>
                    </td>
                </tr>
            </div>
        )
    }
}
