import React, { Component } from 'react'
import Content from './Content';

export default class Review extends Component {
    constructor(){
        super();
        this.state = {
            data: [
                {
                    id: 1,
                    name: "bob"
                },
                {
                    id: 2,
                    name: "jack"
                }
            ],
            d: "",
            n: ""
        }
        this.onChange = this.onChange.bind(this);
    }
    onChange(e){
        this.setState({[e.target.name]: e.target.value});
        console.log(this.state.d)
    }
    addData=()=>{
        this.setState(prevState => ({
            data: [...prevState.data, {"id": this.state.d,
                                    "name": this.state.n}]
          }))
    }
    render() {
        let list = this.state.data.map((d)=>
            <Content key={d.id} user={d}/>
        )
        return (
            <div style={{margin: 300}}>
                <table>
                    {list}
                </table>
                <form>
                    <label for="d">id</label><br></br>
                    <input type="text" name="d" value={this.state.value} onChange={this.onChange}></input><br></br>
                    <label for="d">name</label><br></br>
                    <input type="text" name="n" value={this.state.value} onChange={this.onChange}></input>
                </form>
                <button onClick={this.addData}>Add</button>
            </div>
        )
    }
}
