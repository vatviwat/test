import React, { Component } from 'react'
export default class Content extends Component {
    constructor() {
        super();
        this.state = {
            textColor: "",
            text: "Hide",
            isActive: null,
        }
    }
    onChange=(event)=>{
        if (event !== this.state.isActive) {
            this.setState({isActive: event,textColor: "red",text: "Unhide"})
        } 
        else {
            this.setState({isActive: null,textColor: "",text: "Hide"});
        }
    }
    render() {
        return (
            <div>
                <tr>
                    <td style={{color: this.state.textColor}}>{this.props.user.id}</td>
                    <td style={{color: this.state.textColor}}>{this.props.user.name}</td>
                    <td>
                        <button style={{color: this.state.textColor}} onClick={this.onChange}>{this.state.text}</button>
                    </td>
                </tr>
            </div>
        )
    }
}

