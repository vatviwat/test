import React, { Component } from 'react';
import './App.css';
import Result from './Result';

class App extends Component{
   constructor(){
      super();
      this.state = {
         count: 0
      }
   }
   increasement=()=>{
      this.setState(prevState=>{
         return {count: prevState.count+ 1}
      })
   };
   decreasement=()=>{
      this.setState(prevState=>{
         return {
            count: prevState.count - 1}
      });
   }
   render() {
      return (
         <div style={{textAlign: 'center'}}>
            <Result count={this.state.count}/>
            <button onClick={this.increasement}>Click to increase</button>
            <button onClick={this.decreasement}>Click to decrease</button>
         </div>
      );
   }
 }
export default App;
